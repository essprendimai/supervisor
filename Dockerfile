FROM php:7.2.2-fpm

RUN docker-php-ext-install pdo pdo_mysql pcntl posix

RUN \
  apt-get update && \
  apt-get install -y supervisor && \
  rm -rf /var/lib/apt/lists/*

RUN apt-get install -y python2.7

WORKDIR /etc/supervisor/conf.d

COPY laravel-default.conf /etc/supervisor/conf.d/laravel-default.conf

ENV APP_HOME /home/docker/src

RUN mkdir -p $APP_HOME/crawler \
    && mkdir -p $APP_HOME/bin

CMD ["/usr/bin/supervisord", "-n", "-c", "/etc/supervisor/supervisord.conf"]